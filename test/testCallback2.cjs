const callback2 = require("../callback2.cjs")

function callback(err, data) {
    if (err) {
        console.error(err);
    } else {
        console.log(data);
    }
}

callback2("mcu453ed", callback);