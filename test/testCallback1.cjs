const callback1 = require("../callback1.cjs")

function callback(err, data) {
    if (err) {
        console.error(err);
    } else {
        console.log(data);
    }
}

callback1("mcu453ed", callback);
