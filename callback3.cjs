const cards = require('./cards.json')

function callback3(idList, callback) {
    if ( typeof idList !== 'string' || typeof callback !== 'function') {
        console.error('Invalid List ID.');
    } else {
        setTimeout(() => {
            let cardInfo = Object.entries(cards)
                .filter((card) => {
                    return card[0] === idList;
                });
            cardInfo = Object.fromEntries(cardInfo);
            if (cardInfo) {
                callback(null, cardInfo);
            } else {
                callback('List ID not found');
            }
        }, 3 * 1000);
    }
}
module.exports = callback3;