const callback1 = require("./callback1.cjs");
const callback2 = require("./callback2.cjs");
const callback3 = require("./callback3.cjs");

const boards = require("./boards.json");

function callback4(boardName) {
  setTimeout(() => {
    const thanos = boards.find((board) => {
      return board.name === boardName;
    });
    console.log(thanos);
    const idThanos = thanos.id;
    callback1(idThanos, (err, data) => {
      if (err) {
        console.error(new Error(err));
      } else {
        data = data[0];
        const listID = data.id;
        callback2(listID, (err, data) => {
          if (err) {
            console.error(new Error(err));
          } else {
            console.log(data);
            const listName = "Mind";
            const listMind = data[listID].find((boardList) => {
              return boardList.name === listName;
            });
            const cardId = listMind.id;
            callback3(cardId, (err, data) => {
              if (err) {
                console.error(new Error(err));
              } else {
                console.log(data);
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}
module.exports = callback4;
