const boards = require("./boards.json")

function callback1(idBoard, callback) {
    if ( typeof idBoard !== 'string' || typeof callback !== 'function') {
        console.error('Invalid Board ID.');
    } else {
        setTimeout(() => {
            let boardInfo = boards.filter((board) => {
                    return board.id === idBoard;
            });
            if (boardInfo) {
                callback(null, boardInfo);
            } else {
                callback('Board ID not found');
            }
        }, 3 * 1000);
    }
}
module.exports = callback1;

