const lists = require('./lists.json')
function callback2(idBoard, callback) {
    if ( typeof idBoard !== 'string' || typeof callback !== 'function') {
        console.error('Invalid Board ID.');
    } else {
        setTimeout(() => {
            let listInfo = Object.entries(lists)
                .filter((list) => {
                    return list[0] === idBoard;
                });
            listInfo = Object.fromEntries(listInfo);
            if (listInfo) {
                callback(null, listInfo);
            } else {
                callback('Board ID not found');
            }
        }, 3 * 1000);
    }
}
module.exports = callback2;